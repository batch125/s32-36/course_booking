const mongoose = require(`mongoose`);

let courseSchema = new mongoose.Schema({
	courseName:{

		type:String,
		required:[true, `Course is required`]
	},
	courseDesc:{

		type:String,
		required:[true, `Description is required`]
	},
	price:{

		type:Number,
		required:[true, `Price is required`]
	},
	isActive:{

		type:Boolean,
		default: true
	},
	createdOn:{

		type:Date,
		default: new Date()
	},
	enrollees:[
		{
			userId:{

				type:String,
				required:[true, `User ID is required`]
			},
			enrolledOn:{

				type:Date,
				default: new Date()
			}
		
		}]

});


module.exports = mongoose.model(`Course`, courseSchema);