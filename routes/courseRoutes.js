
const express = require('express');
const router = express.Router();
let auth = require('./../auth');

const courseController = require('./../controllers/courseControllers');

//retrieve active courses
router.get('/allActive', (req, res) => {

	courseController.getAllActive().then( result => res.send(result));
})

//retrieve all courses
router.get("/getAllCourses", (req, res) => {

	courseController.getAllCourses().then( result => res.send(result))
})

//retrieve single course
router.get("/:courseId", auth.verify, (req, res) => {

	courseController.singleCourse(req.params).then( result => res.send(result))
})

//update existing course

router.put("/:courseId/edit", auth.verify, (req, res) => {

	courseController.editCourse(req.params.courseId, req.body).then( result => res.send(result))
})

//add course
router.post("/addCourse", auth.verify, (req, res) => {

	courseController.addCourse(req.body).then( result => res.send(result))
})

//archive course
router.put('/:courseId/archive', auth.verify,(req,res)=>{
	//console.log(req.params.courseId)
	courseController.archiveCourse(req.params.courseId).then(result => res.send(result));
})

//unarchive course
router.put('/:courseId/unarchive', auth.verify,(req,res)=>{
	//console.log(req.params.courseId)
	courseController.unarchiveCourse(req.params.courseId).then(result => res.send(result));
})

//delete course
router.delete('/:courseId/delete', auth.verify,(req,res)=>{
	//console.log(req.params.courseId)
	courseController.deleteCourse(req.params.courseId).then(result => res.send(result));
})

// get user
router.get("/:userId", auth.verify, (req, res) => {

	courseController.user(req.params).then( result => res.send(result))
})
module.exports = router;
