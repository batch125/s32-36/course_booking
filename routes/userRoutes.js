
let express = require(`express`);
const router = express.Router();
const userController = require(`./../controllers/userControllers`);
// const courseController = require(`./../controllers/courseControllers`);

const auth = require(`./../auth`)


//email validation/checking if exist
router.post(`/checkEmail`,(req,res)=>{

	userController.checkEmailExist(req.body).then(result =>res.send(result)
	);
})


//user registration
router.post(`/register`,(req,res)=>{

	userController.register(req.body).then(result =>res.send(result)
	);

})

//login

router.post(`/login`,(req,res)=>{


	userController.login(req.body).then(result =>res.send(result)
	);
})

// user profile

router.get(`/details`, auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization)
	console.log(userData);
	

	userController.getProfile(userData.id).then(result => res.send(result))
})

//enrollements

router.post(`/enroll`, auth.verify,(req,res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId:req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result))
	console.log(data)
})
module.exports = router;
