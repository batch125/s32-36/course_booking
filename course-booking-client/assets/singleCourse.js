
// console.log(window.location.search);
let params = new URLSearchParams(window.location.search); 
//method of URLSearchParams
	//URLSearchParams.get()
let courseId = params.get('courseId');
console.log(courseId);

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollmentContainer = document.querySelector('#enrollmentContainer');

let token = localStorage.getItem('token');

fetch(`http://localhost:3000/api/courses/${courseId}`,
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then( result => result.json())
.then( result => {

	// console.log(result)	//single course document/object


	//to show course details
	courseName.innerHTML = result.courseName
	courseDesc.innerHTML = result.courseDesc
	coursePrice.innerHTML = result.price
	enrollmentContainer.innerHTML =
	`
		<button class="btn btn-primary btn-block" id="enrollButton">Enroll</button>
	`
	let enrollButton = document.querySelector(`#enrollButton`);

	enrollButton.addEventListener("click",()=>{
		fetch(`http://localhost:3000/api/users/enroll`,
		{
			method:"POST",
			headers:{
				"Content-Type": "application/json",
				"Authorization":`Bearer ${token}`
			},
			body:JSON.stringify({
				courseId:courseId //or result._id,

			})
		}).then(result=> result.json())
		.then(result=>{
			// console.log(result)
			if(result){
				alert(`Enrolled Successfully`);
				window.location.replace(`./courses.html`) 
			}
			else{
				alert(`Something went wrong`)
			}
		})
	})

})
