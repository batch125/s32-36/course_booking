//archive course
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
console.log(courseId);

let token = localStorage.getItem('token');

fetch(`http://localhost:3000/api/courses/${courseId}/archive`,
	{
		method: "PUT",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	console.log(result)

	if (result === true) {
		alert(`Course Succesfully Archived!`)
		window.location.replace(`./courses.html`)
	} else {
		alert(`Something went wrong.`);
	}
})