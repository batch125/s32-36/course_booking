
let token = localStorage.getItem('token')

let name = document.querySelector('#name');
let email = document.querySelector('#email');
let mobileNo = document.querySelector('#mobileNo');
let editButton = document.querySelector('#editButton');
let enrollmentContainer = document.querySelector(`#enrollContainer`);

fetch('http://localhost:3000/api/users/details',
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	console.log(result)

	// if(result){
		name.innerHTML = `${result.firstName} ${result.lastName}`
		email.innerHTML = result.email
		mobileNo.innerHTML = result.mobileNo
		editButton.innerHTML =
		`
			<div class="mb-2">
				<a href="./editProfile.html" class="btn btn-primary">Edit Profile</a>
			</div>
		`
		result.enrollments.forEach( course => {

		let courseId = course.courseId

		fetch(`http://localhost:3000/api/courses/${courseId}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(course)
			console.log(result)

			let card = 
			`
			<div class="card my-5">
				<div class"card-body">
					<div class="card-title">${result.courseName}</div>
					<div class="card-title">${result.courseDesc}</div>
					<div class="card-title">${course.enrolledOn}</div>
					<div class="card-title">${course.status}</div>
				</div>
			</div>
			`

			enrollContainer.insertAdjacentHTML("beforeend", card);
		})
	})
})


