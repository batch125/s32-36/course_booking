
//target the form
let createCourse = document.querySelector('#createCourse');
let token = localStorage.getItem(`token`);

//listen to an even (such as submit)
createCourse.addEventListener("submit", (event) => {
	event.preventDefault();

let courseName = document.querySelector('#courseName').value
let coursePrice = document.querySelector('#coursePrice').value
let courseDesc = document.querySelector('#courseDesc').value

	if(courseName !== "" && coursePrice !== "" && courseDesc !== ""){
	//use fetch to send the data to the server
	console.log("test")
		fetch("http://localhost:3000/api/courses/addCourse", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseName: courseName,
					price: coursePrice,
					courseDesc: courseDesc
				})
			}
		)
		.then( result => result.json())
		.then( result => {
			console.log(result)

			if(result){
				alert("Course Succesfully Created");

				window.location.replace('./courses.html')
			} else {
				alert("Course Creation Failed. Something went wrong.")
			}

		})
	}

})
