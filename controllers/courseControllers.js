const Course = require(`./../models/Courses`);


//to get all active courses
module.exports.getActiveCourses = () => {

	return Course.find({isActive: true}).then( result => {
		
		return result
	})
}

//to get ALL courses
module.exports.getAllCourses = () => {
	
	return Course.find().then( result => {
		return result
	})
}

//to get single course id: 6125ba055139757117238d32
module.exports.singleCourse = (params) => {

	console.log(params)
	return Course.findById(params.courseId).then( (result) => {
	return result
	})
}


//to add/create a course
module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({

		courseName: reqBody.courseName,
		courseDesc: reqBody.courseDesc,
		price: reqBody.price
	});
	return newCourse.save().then((savedCourse, error)=>{
		if (error){
			return error
		}
		else{
			return savedCourse
		}

	})
}

//to edit course
module.exports.editCourse = (params, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//Model.method
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

//archive course
module.exports.archiveCourse = (params)=> {

	let updatedActiveCourse = {
		isActive : false
	}

	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then((result, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//unarchive course
module.exports.unarchiveCourse = (params)=> {

	let updatedActiveCourse = {
		isActive : true
	}

	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then((result, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//delete course
module.exports.deleteCourse = (params)=> {

	return Course.findByIdAndDelete(params).then((result, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}