let express = require(`express`);
let mongoose = require(`mongoose`);
const PORT = process.env.PORT || 4000;


let app = express();
const cors = require(`cors`);

//routes
let userRoutes = require(`./routes/userRoutes`)
let courseRoutes = require(`./routes/courseRoutes`)

// middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

mongoose.connect(`mongodb+srv://zuittDBBatch125:70pez0265@cluster0.l6bhm.mongodb.net/course_booking?retryWrites=true&w=majority`,
	{
			useNewUrlParser:true,
			useUnifiedTopology:true
		}).then(()=> {
			console.log(`Connected to Database.`);
		}).catch((error)=> {
			console.log(error);
		});

app.use(`/api/users`, userRoutes);
app.use(`/api/courses`, courseRoutes);
app.listen(PORT,()=>console.log(`Server is running on port ${PORT}`));
